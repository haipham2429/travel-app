﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V4.Widget;
using SupportFragment = Android.Support.V4.App.Fragment;
using SupportFragmentManager = Android.Support.V4.App.FragmentManager;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using SupportActionBar = Android.Support.V7.App.ActionBar;
using Android.Support.V7.App;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.App;
using System.Collections.Generic;
using Java.Lang;
using FragmentController = Android.Support.V4.App.FragmentController;
using System.Collections;

namespace Travel_Trainning_App
{
    [Activity(Theme="@style/MyTheme" ,Label = "@string/app_name", MainLauncher = true)]

    public class MainActivity : AppCompatActivity , Android.Support.Design.Widget.NavigationView.IOnNavigationItemSelectedListener
    {
        public SupportFragment mCurrentFragment;
        public SupportFragment Fragment1;
        public SupportFragment Fragment2;
        public SupportFragment Fragment3;
        private Stack<SupportFragment> mStackFragment;
        private DrawerLayout mDrawerlayout;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.activity_main);
            mDrawerlayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            mCurrentFragment = Fragment1;
            mStackFragment = new Stack<SupportFragment>();

            SupportActionBar ab = SupportActionBar;
            ab.SetHomeAsUpIndicator(Resource.Drawable.abc_ic_menu_copy_mtrl_am_alpha);
            ab.SetDisplayHomeAsUpEnabled(true);

            mDrawerlayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            NavigationView navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            
            if (navigationView != null)
            {
                SetUpDrawerContent(navigationView);
            }

            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);
            navigationView.SetNavigationItemSelectedListener(this);
            SupportFragmentManager.BeginTransaction().AddToBackStack("tag");
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            mDrawerlayout.OpenDrawer((int)GravityFlags.Left);
            return base.OnOptionsItemSelected(item);
        }



        private void SetUpDrawerContent(NavigationView navigationView)
        {
            navigationView.NavigationItemSelected += (object sender, NavigationView.NavigationItemSelectedEventArgs e) =>
            {
                e.MenuItem.SetChecked(true);
                mDrawerlayout.CloseDrawers();
            };
        }

        public bool OnNavigationItemSelected(IMenuItem menuItem)
        {
            switch (menuItem.ItemId)
            {
                case Resource.Id.nav_main:
                    SupportFragmentManager.BeginTransaction().Replace(Resource.Id.fragmentContainer, new fragment1()).Commit();
                    break;
                case Resource.Id.nav_mytrip:
                    SupportFragmentManager.BeginTransaction().Replace(Resource.Id.fragmentContainer, new fragment2()).Commit();
                    break;
                case Resource.Id.nav_share:
                    SupportFragmentManager.BeginTransaction().Replace(Resource.Id.fragmentContainer, new fragment3()).Commit();
                    break;

                default:
                    break;

            }
            mDrawerlayout.CloseDrawers();
            return true;
        }
        
        public override void OnBackPressed()
        {

                base.OnBackPressed();

        }
        public void ReplaceFragment(SupportFragment fragment)
        {
            if (fragment.IsVisible)
                return;
            var trans = SupportFragmentManager.BeginTransaction();
            trans.Replace(Resource.Id.fragmentContainer, fragment);
            trans.AddToBackStack(null);
            trans.Commit();

            mCurrentFragment = fragment;

        }
    }
}